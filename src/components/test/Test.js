import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { loginRequest } from '../../store/actions/authorization.action'
import Test from '../../layout/test/Test';


const mapStateToProps = state => state;

// const mapDispatchToProps = dispatch => {
//     return {
//         getDataRequested: () => dispatch(authorizationActions),
//     };
// };

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        loginRequest
    }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps) (
    props => {
        const { loginRequest } = props;

        return (
            <Test onClick={loginRequest}>ELO</Test>
        )
    
})