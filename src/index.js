import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from 'styled-components'
import { Provider } from 'react-redux';

import Test from './components/test/Test'
import store from './store/store'

const theme = {
    fontSize: '20px',
}


function App() {
    return (
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <div>
                    Click Me
                </div>
                <Test />
            </ThemeProvider>
        </Provider>
    );
}

ReactDOM.render(<App />, document.getElementById('root'));

// serviceWorker.unregister();
