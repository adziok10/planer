import { ofType } from 'redux-observable';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';


import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE } from '../actions/authorization.action'

export const loginEpic = action$ => action$.pipe(
    ofType(LOGIN_REQUEST),
    switchMap(() => ajax
        .getJSON('https://jsonplaceholder.typicode.com/todos/1')
    ),
    map(payload => ({ type: LOGIN_SUCCESS, payload: payload })),
    catchError(error => ({ type: LOGIN_FAILURE, payload: error.message }))
)