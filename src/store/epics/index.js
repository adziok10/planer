import { combineEpics } from 'redux-observable';

import { loginEpic } from './authorization.epic';

export default combineEpics(
    loginEpic,
);