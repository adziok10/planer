import { LOGIN_FAILURE, LOGIN_SUCCESS, LOGIN_REQUEST } from '../actions/authorization.action'

export const authorizationReducer = (state = [], { type, payload }) => {

    switch (type) {
        case LOGIN_REQUEST:
            return { ...state, fetch: { ...state.fetch, pending: true } }

        case LOGIN_FAILURE:
            return { ...state, fetch: { completed: true, pending: false, error: payload }  }

        case LOGIN_SUCCESS:
            return { ...state, completed: true, fetch: { ...state.fetch, completed: true, pending: false, }, ...payload }
    
        default:
            return state
    }

}
