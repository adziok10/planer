import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';

import rootEpic from './epics'
import reducers from './reducers';

const epicMiddleware = createEpicMiddleware();

const store = createStore(
    reducers,
    applyMiddleware(epicMiddleware),
);

epicMiddleware.run(rootEpic);

export default store;